1.  [x] Build resume in HTML
2.  [x] Style the website with CSS
3.  [ ] Host website in S3 bucket
4.  [ ] Enable static website using HTTPS
5.  [ ] register a custom DNS domain for website
6.  [x] Build visitor counter using JavaScript
7.  [ ] Provision DynamoDB for visitor count retrieval and storage
8.  [ ] Provision API Gateway and Lambda function to talk to DB backend
9.  [ ] Build Lamba function using Python
10. [ ] Write tests for Lambda function code
11. [ ] Provision resources using terraform 
12. [x] Push code to GitHub repo 
13. [ ] Write GitHub actions for pushing updates (backend)
14. [x] Push website code to separate github repo (frontend)